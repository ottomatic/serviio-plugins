@ECHO OFF

REM Replaces escaped commas in the -i parameter passed to FFMPeg with actual commas
REM
REM This is needed for Serviio 1.0.x (and possibly earlier) but should not be needed in Serviio 1.1 or newer.
REM
REM By Otto Dandenell 2012-11-18
REM 
REM This script is a mashup of http://skypher.com/index.php/2010/08/17/batch-command-line-arguments/
REM and http://stackoverflow.com/questions/5273937/how-to-replace-substrings-in-windows-batch-file

SET PATH_TO_FFMPEG="C:\Program Files\Serviio\lib\FFMPeg.exe"

SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
 
SET NEWSWITCHES = ""
SET ISISWITCHVALUE = ""

SET ARGV=.%*
CALL :PARSE_ARGV

IF ERRORLEVEL 1 (
  ECHO Cannot parse arguments
  ENDLOCAL
  EXIT /B 1
)

SET ISISWITCHVALUE=""
REM ECHO Arguments count = !ARGC!
FOR /L %%I IN (1,1,!ARGC!) DO (
  CALL :GETARG %%I ARGI
REM   ECHO Argument #%%I = [ !ARGI! ]

  IF !ISISWITCHVALUE!=="" (
      REM Ordinary switch / value
REM       ECHO Ordinary value
      IF !NEWSWITCHES!=="" (
REM	ECHO INITIATING NEWSWITCHES
          SET NEWSWITCHES=!ARGI!
      ) ELSE (
          SET NEWSWITCHES=!NEWSWITCHES! !ARGI!
      )
      IF !ARGI!==-i (
REM	ECHO SETTING ISISWITCHVALUE="1"
	SET ISISWITCHVALUE="1"
      ) ELSE (
REM	ECHO SETTING ISISWITCHVALUE=""
	SET ISISWITCHVALUE=""
      )
  )  ELSE (
      REM This is the Url for FFMPEG. replace %2c with ,
REM      ECHO replacing %%2c with comma
      SET modified=!ARGI:%%2c=,!
REM      ECHO modified = [ !modified! ]
      SET NEWSWITCHES=!NEWSWITCHES! !modified!
  )

REM  ECHO NEWSWITCHES = [ !NEWSWITCHES! ]

)

REM ECHO !PATH_TO_FFMPEG! !NEWSWITCHES!
!PATH_TO_FFMPEG! !NEWSWITCHES!

ENDLOCAL
EXIT /B 0

:GETARG
  SET %2=!ARG%1!
  SET %2_=!ARG%1_!
  SET %2Q=!ARG%1Q!
  EXIT /B 0

:GETARGS
  SET %3=
  FOR /L %%I IN (%1,1,%2) DO (
    IF %%I == %1 (
      SET %3=!ARG%%I!
    ) ELSE (
      SET %3=!%3! !ARG%%I!
    )
  )
  EXIT /B 0
 

:PARSE_ARGV
  SET PARSE_ARGV_ARG=[]
  SET PARSE_ARGV_END=FALSE
  SET PARSE_ARGV_INSIDE_QUOTES=FALSE
  SET /A ARGC = 0
  SET /A PARSE_ARGV_INDEX=1
  :PARSE_ARGV_LOOP
  CALL :PARSE_ARGV_CHAR !PARSE_ARGV_INDEX! "%%ARGV:~!PARSE_ARGV_INDEX!,1%%"
  IF ERRORLEVEL 1 (
    EXIT /B 1
  )
  IF !PARSE_ARGV_END! == TRUE (
    EXIT /B 0
  )
  SET /A PARSE_ARGV_INDEX=!PARSE_ARGV_INDEX! + 1
  GOTO :PARSE_ARGV_LOOP
 
  :PARSE_ARGV_CHAR
    IF ^%~2 == ^" (
      SET PARSE_ARGV_END=FALSE
      SET PARSE_ARGV_ARG=.%PARSE_ARGV_ARG:~1,-1%%~2.
      IF !PARSE_ARGV_INSIDE_QUOTES! == TRUE (
        SET PARSE_ARGV_INSIDE_QUOTES=FALSE
      ) ELSE (
        SET PARSE_ARGV_INSIDE_QUOTES=TRUE
      )
      EXIT /B 0
    )
    IF %2 == "" (
      IF !PARSE_ARGV_INSIDE_QUOTES! == TRUE (
        EXIT /B 1
      )
      SET PARSE_ARGV_END=TRUE
    ) ELSE IF NOT "%~2!PARSE_ARGV_INSIDE_QUOTES!" == " FALSE" (
      SET PARSE_ARGV_ARG=[%PARSE_ARGV_ARG:~1,-1%%~2]
      EXIT /B 0
    )
    IF NOT !PARSE_ARGV_INDEX! == 1 (
      SET /A ARGC = !ARGC! + 1
      SET ARG!ARGC!=%PARSE_ARGV_ARG:~1,-1%
      IF ^%PARSE_ARGV_ARG:~1,1% == ^" (
        SET ARG!ARGC!_=%PARSE_ARGV_ARG:~2,-2%
        SET ARG!ARGC!Q=%PARSE_ARGV_ARG:~1,-1%
      ) ELSE (
        SET ARG!ARGC!_=%PARSE_ARGV_ARG:~1,-1%
        SET ARG!ARGC!Q="%PARSE_ARGV_ARG:~1,-1%"
      )
      SET PARSE_ARGV_ARG=[]
      SET PARSE_ARGV_INSIDE_QUOTES=FALSE
    )
    EXIT /B 0
