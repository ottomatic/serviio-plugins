#!/usr/bin/env bash
PATH_TO_FFMPEG=/usr/local/bin/ffmpeg

PRESWITCHES=""
FILENAME=""
POSTSWITCHES=""
INPUT_FOUND=false

COUNT=$#
for ((INDEX=0; INDEX<COUNT; ++INDEX))
do
   if $INPUT_FOUND
   then
      POSTSWITCHES="$POSTSWITCHES $1"
   else
      if [ "$1" == "-i" ]
      then
         # Unescape the escaped commas
         FILENAME=`echo "$2" | sed -e s/%2c/,/g`

         INPUT_FOUND=true
         shift
      else
         # Add extra switches
         PRESWITCHES="$PRESWITCHES $1"
      fi
   fi

   shift
done

#echo $PATH_TO_FFMPEG $PRESWITCHES -i \"$FILENAME\" $POSTSWITCHES
$PATH_TO_FFMPEG $PRESWITCHES -i "$FILENAME" $POSTSWITCHES