#!/usr/bin/env bash
PATH_TO_FFMPEG=/usr/local/bin/ffmpeg

PRESWITCHES=""
FILENAME=""
POSTSWITCHES=""
INPUT_FOUND=false

COUNT=$#
for ((INDEX=0; INDEX<COUNT; ++INDEX))
do
   if [ "$1" == "-bsf:v" ]
   then
       if [ "$2" == "h264_mp4toannexb" ]
       then
           #remove the option + value
           shift
       else
           # don't remove the option continue parsing.
           if $INPUT_FOUND
           then
              # Add extra switches
              POSTSWITCHES="$POSTSWITCHES $1"
           else
              # Add extra switches
              PRESWITCHES="$PRESWITCHES $1"
           fi	       
       fi
   else
       if $INPUT_FOUND
       then
          POSTSWITCHES="$POSTSWITCHES $1"
       else
          if [ "$1" == "-i" ]
          then
             # Unescape the escaped commas
             FILENAME="$2"

             INPUT_FOUND=true
             shift
          else
             # Add extra switches
             PRESWITCHES="$PRESWITCHES $1"
          fi
       fi
   fi
   shift
done

#echo $PATH_TO_FFMPEG $PRESWITCHES -i \"$FILENAME\" $POSTSWITCHES
$PATH_TO_FFMPEG $PRESWITCHES -i "$FILENAME" $POSTSWITCHES