import java.text.SimpleDateFormat;
import org.serviio.library.metadata.*
import org.serviio.library.online.*
import org.apache.commons.lang.*

/**
 * oppetarkiv.se content URL extractor plugin. 
 * 
 * ##############################################
 * Resource URL instructions
 * ##############################################
 * 
 * This is a plugin of the type "Web Resource" ("Webbresurs").
 *
 * You may add individual shows as individual feeds, e.g.:
 *     http://www.oppetarkiv.se/etikett/titel/Beppes_godnattstund/
 *   
 *     This should give you all the episodes of Beppes godnattstund.
 *
 * OR: You may add a "letter url" giving you all the episodes of
 *     all the shows beginning with that letter. E.g.:
 *     http://www.oppetarkiv.se/program#A
 *     
 *     The episodes will be fetched recursively for each show but
 *     will be presented in a non-hierarchical list since the Serviio
 *     plugin architecture does not support the creation of recursive
 *     folders "on the fly".
 *
 *     You will find all such special section letters at the url:
 *     http://www.oppetarkiv.se/program
 *     
 * ##############################################
 * Version history
 * ##############################################
 *
 * @version 7
 *
 * Version 7: Adjusted for altered markup at the site.
 *            Updated documentation to reflect new standard links for letter url:s
 *            (The new version is backwards compatible with previously registered 
 *             letter urls) 
 * 
 * Version 6: Correct retrieval of "letter URL:s" whose letter requires HTML escaping
 * such as http://www.oppetarkiv.se/kategori/titel#�
 *
 * Version 5: Better resource URL instructions
 * Retrieval of more than 18 episodes per show.
 * 
 * Version 4: Fixed bug caused by HTML escape sequences in URL:s.
 * 
 * Version 3: Added support for feeds on the format:
 * http://www.oppetarkiv.se/kategori/titel#A
 * which lists all episodes of all shows which begin with that letter.
 *
 *
 * ##############################################
 * Credits
 * ############################################## 
 *
 * @author Otto Dandenell 
 * 
 */
class OppetArkiv extends org.serviio.library.online.WebResourceUrlExtractor {
    final VALID_RESOURCE_URL = '^http(s)?://www.oppetarkiv.se/.*$'
    final TITEL_BY_LETTER_URL = '^http(?:s)?://www.oppetarkiv.se/(?:kategori/titel|program)#(.*)$'

    final int VERSION = 7;
    
    final boolean debug = false;
    
    private static SimpleDateFormat df    = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mmz" )
    
    private static SimpleDateFormat dfHourMinute = new SimpleDateFormat( "HH:mm" )

    String getExtractorName() {
        return getClass().getName()
    }

    int getVersion() {
        return VERSION
    }
    
    boolean extractorMatches(URL resourceUrl) {
        return resourceUrl ==~ VALID_RESOURCE_URL
    }

    WebResourceContainer extractItems(URL resourceUrl, int maxItemsToRetrieve) {
        log( getExtractorName() + ': extracting items for url: ' +  resourceUrl + ' , maxItemsToRetrieve: ' + maxItemsToRetrieve)
        def resourceHtml
        
        /* Find out if the resourceUrl is configured to fetch clips or shows or both. */
        def matcherServiioClips = (resourceUrl =~ '.*(?:[?|&]serviioclips=)([^&$]*)')
        def matcherServiioEpisodes = (resourceUrl =~ '.*(?:[?|&]serviioepisodes=)([^&$]*)')
        
        def blnIncludeServiioClips = false
        def blnIncludeServiioEpisodes = true
        def blnIncludeServiioLive = true
        
        if (matcherServiioClips != null && matcherServiioClips.size() > 0)
        {
            switch (matcherServiioClips[0][1])
            {
                case "1":
                case "true":
                case "yes":
                    blnIncludeServiioClips = true
                    break
                default:
                    blnIncludeServiioClips = false
            }
        }
        if (matcherServiioEpisodes != null && matcherServiioEpisodes.size() > 0)
        {
            switch (matcherServiioEpisodes[0][1])
            {
                case "1":
                case "true":
                case "yes":
                    blnIncludeServiioEpisodes = true
                    break
                default:
                    blnIncludeServiioEpisodes = false
            }
        }
        
        /* Remove serviio request parameters */
        def properResourceUrl = (resourceUrl =~ '(?:serviioclips=[^&]*&?)').replaceAll('')
        properResourceUrl = (properResourceUrl =~ '(?:serviioepisodes=[^&]*&?)').replaceAll('')
                      
        /* Remove trailing ampersands and question marks. */
        while (properResourceUrl ==~ /.*[\?&]$/)
        {
            properResourceUrl = (properResourceUrl =~ /[\?&](?:$)/).replaceAll('')
        }
                
        def url = new URL(''+properResourceUrl)        
        
        def connection = url.openConnection()
        if(connection.responseCode == 200){
            resourceHtml = connection.content.text
        }
        else{
            return null
        }
               
        def itemsAdded = 0;
        List<WebResourceItem> items = [] 

        /* IF we have a "letter" URL we sniff each show recursively */
        if (properResourceUrl ==~ TITEL_BY_LETTER_URL)
        {
            def letterSectionMatch = (properResourceUrl =~ TITEL_BY_LETTER_URL)
            def selectedLetter = letterSectionMatch[0][1]

            log('Found feed URL expecting to list all episodes of all shows whose title begins with ' + selectedLetter + '. Attempting to download and parse each show recursively...')
            if (debug) println 'Found feed URL expecting to list all episodes of all shows whose title begins with ' + selectedLetter + '. Attempting to download and parse each show recursively...'
            
            // The anchors are HTML-escaped
            def selectedLetterEscaped = org.apache.commons.lang.StringEscapeUtils.escapeHtml(selectedLetter)

            def letterSectionMatcherExpression = '(?s)<a id=\"' + selectedLetterEscaped + '\".*?</section>'
            
            def letterSectionMatcher = (resourceHtml =~ letterSectionMatcherExpression)
            if (letterSectionMatcher != null && letterSectionMatcher.getCount() > 0)
            {
                def letterSection = letterSectionMatcher[0]
                if (debug) println 'letterSection: ' + letterSection
                
                def letterTitleHrefMatcher = (letterSection =~ '<li [^>]*?class=\"[^\"]*?svtoa-anchor-list-item.*?<a .*? href=\"([^\"]*?)\"[^>]*?>(.*?)</a>')
                if (debug) println 'letterTitleHrefMatcher.getCount(): ' + letterTitleHrefMatcher.getCount()
                
                if (letterTitleHrefMatcher != null && letterTitleHrefMatcher.getCount() > 0)
                {
                    // instantiate a container which will hold items for all shows recursively.
                    def webResourceContainerForLetter = new WebResourceContainer()

                    // Use a generic logo.             
//                    def oppetArkivLogoMatcher = (resourceHtml =~ '<img class=\"[^\"]*?svt-print-logo\" src=\"([^\"]*?)\"')
                    def oppetArkivLogoMatcher = (resourceHtml =~ '<link rel="shortcut icon" .*?src=\"([^\"]*?)\"')
                    if (oppetArkivLogoMatcher != null && oppetArkivLogoMatcher.getCount() > 0)
                    {
                        def oppetArkivLogo = oppetArkivLogoMatcher[0][1]
                        if (!(oppetArkivLogo ==~ '^http.*'))
                        {
                            oppetArkivLogo = 'http://www.oppetarkiv.se' + oppetArkivLogo
                        }
                        webResourceContainerForLetter.thumbnailUrl = oppetArkivLogo
                    }
                    
                    // Use a generic title for the selected letter
                    webResourceContainerForLetter.title = '�ppet arkiv: ' + selectedLetter

                    // Get the items for each show and add to the container.
                    for(int i = 0; i < letterTitleHrefMatcher.getCount() && (maxItemsToRetrieve == -1 || itemsAdded < maxItemsToRetrieve) ; i++ ) {
                        def letterTitleUrl = letterTitleHrefMatcher[i][1]
                        if (!(letterTitleUrl ==~ '^http.*'))
                        {
                            letterTitleUrl = 'http://www.oppetarkiv.se' + letterTitleUrl
                        }
                        def letterTitleName = org.apache.commons.lang.StringEscapeUtils.unescapeHtml(letterTitleHrefMatcher[i][2])
                        // There seems to be some html-escaped ampersands in URLS which cause problems if not unescaped.
                        def letterTitleUrlUnescaped =  org.apache.commons.lang.StringEscapeUtils.unescapeHtml(letterTitleUrl)
                        if (debug) println 'letterTitleUrl ' + i + ': ' + letterTitleUrl + ', name: ' + letterTitleName
                        if (debug) println 'letterTitleUrlUnescaped: ' + letterTitleUrlUnescaped
                        WebResourceContainer titleContainer = extractItems(new URL(letterTitleUrlUnescaped), maxItemsToRetrieve)
                        
                        // Add all the items 
                        //webResourceContainerForLetter.items.AddAll(titleContainer.items)
                        if (titleContainer.items.size() > 0)
                        {
                            for (int j = 0; j < titleContainer.items.size() && (maxItemsToRetrieve == -1 || itemsAdded < maxItemsToRetrieve); j++) 
                            {
                                def item = titleContainer.items[j];
                                // Modify the title to make sure the name of the show is included, not just the episode name
                                item.title = letterTitleName + ': ' + item.title
                                items << item
                                itemsAdded++;
                            }
                        }
                    }
                    webResourceContainerForLetter.items = items
                                        
                    log( getExtractorName() + ': finished extracting items for url: ' +  resourceUrl + ' . Found ' + itemsAdded + ' items' )
                    return webResourceContainerForLetter
                }
            }
            return null
        }

        // find out the title
        def title
        //<title>Barn | SVT Play</title>
        def matcherTitle = (resourceHtml =~ '<title>(.*?)</title>')
        if(matcherTitle.size() > 0)
        {
            title = StringEscapeUtils.unescapeHtml(matcherTitle[0][1])
            if (debug) println 'title - ' + title
        }
        
        // Find a category thumbnail by matching known categories
        def thumbnailUrl
        // println 'thumbnailUrl - ' + thumbnailUrl
        
        // Parse items from markup, return only max items
        /*
            <article class="svtUnit svtNth-3 svtMediaBlock svtMediaBlock-oa svt-margin-bottom-15px">
            <div class="svt-display-table-xs">
                <div class="svt-table-cell-left-xs svtJsClickArea">
                    <figure class="svtMediaBlockFig-M svtMBFig-L-O-O-O svt-text-margin-medium  ">
                        <noscript>
    <img class="oaImg" alt="Avsnitt 2 av 5: Segment" src="http://www.svt.se/oppet-arkiv-pub/cachable_image/1383051781000/incoming/article1564146.svt/ALTERNATES/medium/default_title"/>
</noscript>
<img class="oaImg svtHide-No-Js" data-responsive-maxsize="medium" data-aspect-ratio="16_9" data-imagename="http://www.svt.se/oppet-arkiv-pub/cachable_image/1383051781000/incoming/article1564146.svt/ALTERNATES/medium/default_title" alt="Avsnitt 2 av 5: Segment" src="/public/images/responsiveimage/16_9-cb6c2b19bc81180622d198c1a3268b14.png"/>
                                            </figure>
                    <div class="svt-overflow-hidden">
                                                    <h2 class="svt-vignette-small svt-text-margin-small">Sk&auml;ggen</h2>
                                                                        <h3 class="svt-heading-xs svt-text-margin-medium "><a title="Avsnitt 2 av 5: Segment" class="svtLink-Discreet-THEMED svtJsLoadHref" href="http://www.oppetarkiv.se/video/1564147/skaggen-avsnitt-2-av-5">Avsnitt 2 av 5: Segment</a></h3>

                        <p class="svt-text-time svt-color-darkgrey">
                                                    <time datetime="1963-11-10T13:00+0100">10 november 1963</time>
                                                                        </p>
                    </div>
                </div>
                <div class="svt-table-cell-right-xs svtoa-js-load-info-link svt-pointer-cursor svtoa-position-bottom-right-gt-xs svt-padding-top-10px-padding-left-10px" data-source="/video/details/1564147/">
                    <a href="#" class="svtHide-No-Js svtoa-info-button svtIcon-small-right svtoa-icon-info" role="button">
                        <span class="svtVisuallyhidden">Visa mer information</span>
                    </a>
                </div>
            </div>
        </article>
            
            <article class="svtUnit svtNth-1 svtMediaBlock svtMediaBlock-oa svt-margin-bottom-15px">
            <div class="svt-display-table-xs">
                <div class="svt-table-cell-left-xs svtJsClickArea">
                    <figure class="svtMediaBlockFig-M svtMBFig-L-O-O-O svt-text-margin-medium  ">
                        <noscript>
    <img class="oaImg" alt="Glaset i &ouml;rat" src="http://www.svt.se/oppet-arkiv-pub/cachable_image/1366273741000/incoming/article1100192.svt/ALTERNATES/medium/glaset.jpg"/>
</noscript>
<img class="oaImg svtHide-No-Js" data-responsive-maxsize="medium" data-aspect-ratio="16_9" data-imagename="http://www.svt.se/oppet-arkiv-pub/cachable_image/1366273741000/incoming/article1100192.svt/ALTERNATES/medium/glaset.jpg" alt="Glaset i &ouml;rat" src="/public/images/responsiveimage/16_9.png"/>
                                            </figure>
                    <div class="svt-overflow-hidden">
                                                                            <h2 class="svt-heading-xs svt-text-margin-medium"><a title="Glaset i &ouml;rat" class="svtLink-Discreet-THEMED svtJsLoadHref" href="http://www.oppetarkiv.se/video/1069186/">Glaset i &ouml;rat</a></h2>
                                                <p class="svt-text-time svt-color-darkgrey">
                            <time datetime="1974-10-11T23:00+0100">11 oktober 1974</time>
                        </p>
                    </div>
                </div>
                <div class="svt-table-cell-right-xs svtoa-js-load-info-link svt-pointer-cursor svtoa-position-bottom-right-gt-xs svt-padding-top-10px-padding-left-10px" data-source="/video/details/1069186/">
                    <a href="#" class="svtHide-No-Js svtoa-info-button svtIcon-small-right svtoa-icon-info" role="button">
                        <span class="svtVisuallyhidden">Visa mer information</span>
                    </a>
                </div>
            </div>
        </article>
        */
        
        // Episodes or clips or both? They are encapsulated by divs with the data-tabname attribute.
        def strServiioEpisodesHTML = ""
        if (blnIncludeServiioEpisodes)
        {
            // Include episodes
//            def episodesMatcher =  resourceHtml =~ '(?s)<div [^>]*class="svt-container-main"[^>]*?>.*'
            def episodesMatcher =  resourceHtml =~ '(?s)<div [^>]*role="main"[^>]*?>.*'
            if (episodesMatcher != null && episodesMatcher.getCount() > 0)
            {
                strServiioEpisodesHTML = episodesMatcher[0]
            }
        }
        if (debug) println 'strServiioEpisodesHTML: ' + strServiioEpisodesHTML
        
        def strServiioClipsHTML = ""
        if (blnIncludeServiioClips)
        {
            // Include episodes
            def clipsMatcher =  resourceHtml =~ '(?s)<div [^>]*data-tabname="clips"[^>]*?>.*?(?:<div [^>]*data-tabname=|</body>)'
            if (clipsMatcher != null && clipsMatcher.getCount() > 0)
            {
                strServiioClipsHTML = clipsMatcher[0]
            }
        }
        if (debug) println 'strServiioClipsHTML: ' + strServiioClipsHTML

        // We put live feeds at the top, clips at the bottom.
        def strNonLiveFeedHTML = strServiioEpisodesHTML + strServiioClipsHTML        
        if (debug)  println 'strNonLiveFeedHTML: ' + strNonLiveFeedHTML
        
        def articleMatches = strNonLiveFeedHTML =~ '(?s)<article [^>]*?>.*?</article>'
        for( int i = 0; i < articleMatches.getCount() && (maxItemsToRetrieve == -1 || itemsAdded < maxItemsToRetrieve) ; i++ ) {
            //println 'articleMatches['+ i+ ']: ' + articleMatches[i]            
            // Find out if this has a playlink
//            def playlinkMatcher = articleMatches[i] =~ '(?s)<h2 [^>]*?><a title=\"([^\"]*?)\" class=\"[^\"]*?svtJsLoadHref\" href=\"([^\"]*?)\".*?</div>'
//            def playlinkMatcher = articleMatches[i] =~ '(?s)<h3 [^>]*?><a title=\"([^\"]*?)\" class=\"[^\"]*?svtJsLoadHref\" href=\"([^\"]*?)\".*?</div>'
            def playlinkMatcher = articleMatches[i] =~ '(?s)<h[23] [^>]*?><a title=\"([^\"]*?)\" class=\"[^\"]*?svtJsLoadHref\" href=\"([^\"]*?)\".*?</a>'
            if (playlinkMatcher != null)
            {
                def webResourceItemTitle
                String strReleaseDate
                Date releaseDate
                def videoArticleThumbnailUrl
                def videoArticleUrl         
                
                def validDate = false       
                
                if (playlinkMatcher.getCount() > 0)
                {
                    // Try parsing the releas date
                    // <time datetime="2012-11-09T18:10+01:00">
                    if (debug) println ' Found matching playlink'
                    def timeMatcher = articleMatches[i] =~ '(?s)<time datetime=\"([^\"]*?)\"'
                    if (timeMatcher != null)
                    {
                        if (timeMatcher.getCount() > 0)
                        {
                            strReleaseDate = timeMatcher[0][1]
                            if (debug) println 'strRelease: ' + strReleaseDate
                            try
                            {
                                releaseDate = GetValidDate(strReleaseDate)
                                validDate = true
                                //log(' releaseDate: ' + releaseDate)
                                 if (debug) println ' releaseDate: ' + releaseDate
                            }
                            catch(Exception ex)
                            {
                                 log('ERROR:  Could not parse release date (strReleaseDate: ' + strReleaseDate + ')')
                                  if (debug) println 'Error parsing date: ' + ex.getMessage()
                            }
                        }
                    }

                    // Try parsing the thumbnail
                    //<img class="oaImg" alt="Glaset i &ouml;rat" src="http://www.svt.se/oppet-arkiv-pub/cachable_image/1366273741000/incoming/article1100192.svt/ALTERNATES/medium/glaset.jpg"/>
                    def thumbnailMatcher = articleMatches[i] =~ '(?s)<img class=\"oaImg\" [^>]*?src=\"([^\"]*?)\"'
                    if (thumbnailMatcher != null)
                    {
                        if (thumbnailMatcher.getCount() > 0)
                        {
                            videoArticleThumbnailUrl = thumbnailMatcher[0][1]
                            if (videoArticleThumbnailUrl != null)
                            {
                                if (videoArticleThumbnailUrl.startsWith("//"))
                                {
                                    videoArticleThumbnailUrl = "http:" + videoArticleThumbnailUrl;
                                }
                            }
                            log(' videoArticleThumbnailUrl: ' + videoArticleThumbnailUrl)
                            if (debug) println ' videoArticleThumbnailUrl: ' + videoArticleThumbnailUrl
                        }
                    }
                    
                    webResourceItemTitle = org.apache.commons.lang.StringEscapeUtils.unescapeHtml(playlinkMatcher[0][1])
                    
                    // There may be a vignette title if this is a category search result.
                    // <h2 class="svt-vignette-small svt-text-margin-small">Sk&auml;ggen</h2>
                    def vignetteTitleMatcher = articleMatches[i] =~ '(?s)<h2 class=\"svt-vignette[^\"]*?\"[^>]*?>([^<]*?)</h2>'
                    if (vignetteTitleMatcher != null && vignetteTitleMatcher.getCount() > 0)
                    {
                        def vignetteTitle = org.apache.commons.lang.StringEscapeUtils.unescapeHtml(vignetteTitleMatcher[0][1])
                        //println 'vignetteTitle: ' + vignetteTitle
                        if (vignetteTitle != null && vignetteTitle.length() > 0)
                            webResourceItemTitle = vignetteTitle + ': ' + webResourceItemTitle
                    }
                    log(' webResourceItemTitle: ' + webResourceItemTitle)
                    //println 'webResourceItemTitle: ' + webResourceItemTitle
                    
//                    videoArticleUrl = 'http://www.svtplay.se' + playlinkMatcher[0][2]
                    videoArticleUrl = playlinkMatcher[0][2]
                    if (videoArticleUrl != null)
                    {
                        if (!videoArticleUrl.startsWith("http"))
                        {
                            videoArticleUrl = "http://www.oppetarkiv.se" + videoArticleUrl;
                        }
                    }
                    log(' videoArticleUrl: ' + videoArticleUrl)
                    if (debug) println 'videoArticleUrl: ' + videoArticleUrl
                    if (debug) println 'videoArticleThumbnailUrl: ' + videoArticleThumbnailUrl
                    
                    WebResourceItem item = new WebResourceItem(title: webResourceItemTitle, additionalInfo: ['videoArticleUrl': videoArticleUrl, 'videoArticleThumbnailUrl': videoArticleThumbnailUrl, 'live': 'false'])
                    if (validDate)
                    {
                        item.releaseDate = releaseDate                        
                    }
                    items << item
                    itemsAdded++             
                }
            }
        }
        
        // It's possible that we have a button linking to an ajax-retrieval of more episodes
        /*
<a href="http://www.oppetarkiv.se/etikett/titel/Fem%20myror%20%C3%A4r%20fler%20%C3%A4n%20fyra%20elefanter/?sida=2&amp;sort=tid_stigande" class="svtoa-button svtCenterUnknown svt-margin-top-20px svtoa-js-search-step-button"  data-target=".svtoa-js-searchlist" data-counter=".svtoa-js-search-result-span">
    <div class="svtCenterUnknownContent">
        <!--span class="svtIcon svtIcon_Open-Arrow"></span-->
        <span class="svtXTextBold">
            <span class="svtXColorDarkLightGrey">Visa fler</span>
        </span>
    </div>
</a>                                        
*/
        def showMoreEpisodesLinkMatcher = (resourceHtml =~ '<a [^>]*?href=\"([^\"]*?)\"[^>]*?class=\"[^\"]*?svtoa-js-search-step-button[^>]*?data-page-dir=\"1\"')
        if (debug) println 'showMoreEpisodesLinkMatcher.getCount(): ' + showMoreEpisodesLinkMatcher.getCount()
                
        if (showMoreEpisodesLinkMatcher != null && showMoreEpisodesLinkMatcher.getCount() > 0) 
        {
            def moreEpisodesUrl = showMoreEpisodesLinkMatcher[0][1]
            if (!(moreEpisodesUrl ==~ '^http.*'))
            {
                moreEpisodesUrl = 'http://www.oppetarkiv.se' + moreEpisodesUrl
            }
            if (debug) println 'moreEpisodesUrl: ' + moreEpisodesUrl
            
            // There seems to be some html-escaped ampersands in URLS which cause problems if not unescaped.
            def moreEpisodesUrlUnescaped =  org.apache.commons.lang.StringEscapeUtils.unescapeHtml(moreEpisodesUrl)
            if (debug) println 'moreEpisodesUrlUnescaped: ' + moreEpisodesUrlUnescaped
            
            def maxMoreItemsToRetrieve = (maxItemsToRetrieve > 0) ? maxItemsToRetrieve - itemsAdded : maxItemsToRetrieve
            if (debug) println 'maxMoreItemsToRetrieve: ' + maxMoreItemsToRetrieve
            
            WebResourceContainer moreEpisodesContainer = extractItems(new URL(moreEpisodesUrlUnescaped), maxMoreItemsToRetrieve)
            
            // Add all the items from the container with more episodes
            if (moreEpisodesContainer.items.size() > 0)
            {
                for (int j = 0; j < moreEpisodesContainer.items.size() && (maxItemsToRetrieve == -1 || itemsAdded < maxItemsToRetrieve); j++) 
                {
                    def item = moreEpisodesContainer.items[j];
                    items << item
                    itemsAdded++;
                }
            }
        }

        
        def webResourceContainer = new WebResourceContainer()
        webResourceContainer.title = title
        webResourceContainer.thumbnailUrl = thumbnailUrl
        webResourceContainer.items = items
        
        log( getExtractorName() + ': finished extracting items for url: ' +  resourceUrl + ' . Found ' + itemsAdded + ' items' )
        return webResourceContainer
        
    }

    ContentURLContainer extractUrl(WebResourceItem item, PreferredQuality requestedQuality) {
    
        log ( getExtractorName() + ': extracting Url for WebResourceItem at ' + item.getAdditionalInfo()['videoArticleUrl'])
        //println getExtractorName() + ': extracting Url for WebResourceItem at ' + item.getAdditionalInfo()['videoArticleUrl']
        // Get the html page which displays the video widget
        def videoArticleUrl = new URL(item.getAdditionalInfo()['videoArticleUrl'])
        def videoArticleContentHtml
        def connection = videoArticleUrl.openConnection()
        if(connection.responseCode == 200){
            videoArticleContentHtml = connection.content.text
        }
        else{
            return null
        }
        
        // Parse the json resource url from the data-json attribute.
        def videoJsonMatcher = (videoArticleContentHtml =~ '(?s)<a [^>]*?id=\"player\"[^>]*?data-json-href=\"([^\\"]*?)\"')
        def videoJsonPath
        def videoJsonUrl
        if (videoJsonMatcher != null && videoJsonMatcher.size() > 0)
        {
            videoJsonUrl = new URL('http://www.oppetarkiv.se' + videoJsonMatcher[0][1] + '?output=json')
        }
        else
        {
            //<a id="player" class="svtplayer playJsRemotePlayer playSvtplayer" data-id="547626"
            def videoArticleMatcher = (videoArticleContentHtml =~ '<a id="player" [^>]*? data-id="([^\\"]*?)"')
            def videoArticleId
            assert videoArticleMatcher != null
            if(videoArticleMatcher.size() > 0)
            {
                videoArticleId = videoArticleMatcher[0][1]
                log(' videoArticleId - ' + videoArticleId)
                //println ' videoArticleId - ' + videoArticleId
            }
            else
            {
            
                log(' No video article id or json href found')
                //println ' No video article id found'
                return null
            }
            videoJsonUrl = new URL('http://www.oppetarkiv.se/video/' + videoArticleId + '?output=json')
        }
        log('videoJsonUrl: ' + videoJsonUrl)
        //println 'videoJsonUrl: ' + videoJsonUrl
        def videoJsonContent
        def connectionJson = videoJsonUrl.openConnection()
        if(connectionJson.responseCode == 200){
            videoJsonContent = connectionJson.content.text
        }
        else{
            return null
        }
        log(' videoJsonContent: ' + videoJsonContent)
        //println ' videoJsonContent: ' + videoJsonContent

        // Find the url of the script from which we can parse the swfUrl
        // <script type="text/javascript" src="/public/2012.65/javascripts/script-built.js"></script>
        def swfUrl
        def scriptPathMatcher = (videoArticleContentHtml =~ '<script [^>]* src="([^\\"]*?\\/script-built\\.js)"')
        def scriptPath
        if(scriptPathMatcher != null && scriptPathMatcher.size() > 0)
        {
            scriptPath = scriptPathMatcher[0][1]
            log(' js scriptPath: ' + scriptPath)
            //println ' js scriptPath: ' + scriptPath
            def scriptContent
            URL scriptUrl = new URL('http://www.oppetarkiv.se' + scriptPath)
            def scriptConnection = scriptUrl.openConnection()
            if(scriptConnection.responseCode == 200){
                scriptContent = scriptConnection.content.text
                //println ' js scriptContent: ' + scriptContent                
                // svtplayer.SVTPlayer.SWF_PATH="/statiskt/swf/video/";svtplayer.SVTPlayer.SPRITE_PATH="/statiskt/skins/svt/css/img/svtplayer/";svtplayer.SVTPlayer.PLAYER_FILE="svtplayer-2012.48.swf";svtplayer.SVTPlayer.BUILD_VERSION="2012.48";    
                def swfUrlMatcher = scriptContent =~ 'svtplayer\\.SVTPlayer\\.PLAYER_SWF="([^\\"]*?)"'
                if(swfUrlMatcher != null && swfUrlMatcher.size() > 0)
                {
                    swfUrl = 'http://www.svtplay.se' + swfUrlMatcher[0][1] 
                    log(' swfUrl: ' + swfUrl)
                }
            }
        }
        else
        {
           log( ' Found no path to js script')
           //println  ' Found no path to js script'
        }        

        def contentUrl = GetBestMatch(videoJsonContent, requestedQuality, videoArticleUrl)  
        if (contentUrl == null)
            return null
                              
//        if (swfUrl != null && contentUrl.startsWith('rtmp')) contentUrl = '"' + contentUrl + ' swfUrl=' + swfUrl + ' swfVfy=1 pageUrl=' + videoArticleUrl + '"'
        if (swfUrl != null && contentUrl.startsWith('rtmp')) contentUrl = contentUrl + ' swfUrl=' + swfUrl + ' swfVfy=1'

        def thumbnail = item.getAdditionalInfo()['videoArticleThumbnailUrl']
        
        def blnIsLive = false
        def strIsLive = item.getAdditionalInfo()['live']
        if (strIsLive != null && strIsLive.equals("true"))
            blnIsLive = true
        
//        println 'blnIsLive: ' + blnIsLive
        
        ContentURLContainer container =  new ContentURLContainer(fileType: MediaFileType.VIDEO, contentUrl: contentUrl, thumbnailUrl: thumbnail, live: blnIsLive)
        if (blnIsLive) {
            // Set a cache key and mark this as expired on its startTime
            def strCacheKey = "" + videoArticleUrl + '_' + requestedQuality
//            println 'strCacheKey: ' + strCacheKey
            container.cacheKey = strCacheKey
            container.expiresOn = item.releaseDate
        }
        return container
    }

    
    String GetBestMatch(String jsonContent, PreferredQuality requestedQuality, URL videoUrl){
        String match = null
        String[] priorityList
        if(requestedQuality == PreferredQuality.HIGH){
            priorityList = ['e','d','c','b','a']
        }else{
            if(requestedQuality == PreferredQuality.MEDIUM){
                priorityList = ['c','b','d','a','e']
            }else{
                // LOW
                priorityList = ['a','b','c','d','e']
            }
        }       
        
        // example of match: rtmp://fl11.c91005.cdn.qbrick.com/91005/_definst_/kluster/20110922/PG-1146034-001A-MITTINATUREN2-02-mp4-e-v1.mp4 
        priorityList.each{
            if(match == null){
                def matcher = (jsonContent =~ '.*"url"\\:"(rtmp.*-'+it+'-v1.*?)","bitrate"\\:')
                if(matcher.size() > 0)
                {
                    match = matcher[0][1]
                    log(' found qbrick rtmp url. Best match for requested quality: ' + match)
                }
            }
        }
        
        if (match == null)
        {
            // This might be an AKAMAI video.
            // {"url":"http://svtplay1n-f.akamaihd.net/z/world/open/20121113/1308899-062A_ABC/REGIONALA_NYHET-062A-abc-4fc14b301c7a9fe6_,900,320,420,620,1660,2760,.mp4.csmil/manifest.f4m","bitrate":0,"playerType":"flash"}
            // {"url":"http://svtplay9i-f.akamaihd.net/i/world/open/20121230/1130774-003A/OLOF_PALME-003A-13e3178289b3be47_,900,320,420,620,1660,2760,.mp4.csmil/master.m3u8","bitrate":0,"playerType":"ios"}
            
                // We prefer the hls stream (ios) because ffmpeg has support for it and it is never encrypted
                def httpMatcher = (jsonContent =~ '\\{"url"\\:[^\\"]?"(http[^\\"]*?)","bitrate"[^}]*?"playerType"\\:"ios"\\}')
                // the flash version is more likely to be unplayable by ffmpeg, but we can try it.
                if(httpMatcher == null || httpMatcher.size() == 0)
                    httpMatcher = (jsonContent =~ '\\{"url"\\:[^\\"]?"(http[^\\"]*?)","bitrate"[^}]*?"playerType"\\:"flash"\\}')
                if(httpMatcher.size() > 0)
                {

                    String[] priorityListHttp
                    if(requestedQuality == PreferredQuality.HIGH){
                        priorityListHttp = ['1280x720','1024x576','768x432','704x396','576x324','512x288','480x270','320x180']
                    }else{
                        if(requestedQuality == PreferredQuality.MEDIUM){
                            priorityListHttp = ['768x432','704x396','576x324','1024x576','512x288','480x270','1280x720','320x180']
                        }else{
                            // LOW
                            priorityListHttp = ['320x180','480x270','512x288','576x324','704x396','768x432','1024x576', '1280x720']
                        }
                    }       

                    def httpMatch = httpMatcher[0][1]
//                    println 'httpMatch: ' + httpMatch
                    
                    if (httpMatch.contains("m3u8"))
                    {
//                        println 'httpMatch �r en m3u8-fil'
                        def m3u8URL = new URL(httpMatch)
                        def m3u8Connection = m3u8URL.openConnection()
                        if (m3u8Connection.responseCode == 200){
                            def  m3u8Content = m3u8Connection.content.text
                            //println 'm3u8Content: ' + m3u8Content
                            List m3u8Lines = m3u8Content.readLines()
                            priorityListHttp.each{
                                
                                if(match == null){
                                    for(int index = 0; index < m3u8Lines.size() - 1; index++)
                                    {
//                                        println '      ' + index + ': ' + m3u8Lines.get(index);
                                        def httpm3u8QualityMatcher = (m3u8Lines.get(index) =~ '#EXT-X-STREAM-INF\\:.*,RESOLUTION=' + it +',')
                                        if(httpm3u8QualityMatcher.size() > 0 )
                                        {
                                            match = m3u8Lines.get(index+1)
                                            
                                            // clean up ugly stuff in the url
                                            match = (match =~ /\\?null=&/).replaceAll('')
                                            match = (match =~ /\\?null=$/).replaceAll('')
                                            match = (match =~ /&id=$/).replaceAll('')
                                            match = (match =~ /\?$/).replaceAll('')
                                            log(' found m3u8 http stream url. Best match for requested quality: ' + match)
//                                            println ' found m3u8 http stream url. Best match for requested quality: ' + match
                                        }
                                    }
                                }                                    
                            }
                        }
                        
                    }
                    
                    if (match == null)
                    {
                        // Anv�nd Pirate API
                    
                        // En bugg i pirateplay API:t g�r att vi beh�ver filtrera bort ovidkommande klipp.
                        // H�r hittar vi en mall f�r vad vi ska leta efter bland de str�mmar som pirateplay f�resl�r
                        def httpMatchIntro = (httpMatch =~ /manifest\.f4m$/).replaceAll('')
                        httpMatchIntro = (httpMatchIntro =~ /master\.m3u8$/).replaceAll('')
                        //println 'httpMatchIntro: ' + httpMatchIntro
                    
                    
                        // Get the url from the pirateplayer API
                        def pirateURL = new URL('http://pirateplay.se/api/get_streams.js?url=' + java.net.URLEncoder.encode(videoUrl.toString()))
                        // println 'pirateURL: ' + pirateURL
                     
                        def pirateContent
                           
                        def pirateConnection = pirateURL.openConnection()
                        if (pirateConnection.responseCode == 200){
                            pirateContent = pirateConnection.content.text    
                            //println 'pirateContent: ' + pirateContent
                                            
                            priorityListHttp.each{
                                if(match == null){
                                    def httpPirateMatcher = (pirateContent =~ '"url"\\:[^\\"]?"(http[^\\"]*?)"[^\\}]*?"quality": "' + it +'"')
                                    if(httpPirateMatcher.size() > 0)
                                    {
                                        for(int i=0; i < httpPirateMatcher.size(); i++)
                                        {
                                            //println 'httpPirateMatcher[i][1]: ' + httpPirateMatcher[i][1]
                                            if (httpMatchIntro.length() > 0 && httpPirateMatcher[i][1].startsWith(httpMatchIntro))
                                            {
                                                match = httpPirateMatcher[i][1]
                                                
                                                // clean up ugly stuff in the url
                                                match = (match =~ /\\?null=&/).replaceAll('')
                                                match = (match =~ /&id=$/).replaceAll('')
                                                log(' found pirateAPI http stream url based matching the svtplay json url entry. Best match for requested quality: ' + match)
                                                //println ' found pirateAPI http url based on svtplay json. Best match for requested quality: ' + match
                                             }
                                        }
                                    }
                                }
                            }
                            
                            if(match == null){
                                priorityListHttp.each{
                                    if(match == null){
                                        def httpPirateMatcher = (pirateContent =~ '"url"\\:[^\\"]?"(http[^\\"]*?)"[^\\}]*?"quality": "' + it +'"')
                                        if(httpPirateMatcher.size() > 0)
                                        {
                                            match = httpPirateMatcher[0][1]
                                            
                                            // clean up ugly stuff in the url
                                            match = (match =~ /\\?null=&/).replaceAll('')
                                            match = (match =~ /&id=$/).replaceAll('')
                                            log(' found http pirateAPI stream url NOT based on svtplay json entry. Best match for requested quality: ' + match)
                                            //println ' found http pirate stream url NOT based on svtplay json entry. Best match for requested quality: ' + match
                                        }
                                    }
                                }
                            }
    
                            if (match == null) {
                                // The stream might be dynamic. Just grab the first one.
                                def defaultMatcher = (pirateContent =~ '"url"\\:[^\\"]?"(http[^\\"]*?)"')
                                if(defaultMatcher.size() > 0)
                                {
                                    match = defaultMatcher[0][1]
                                    
                                    // clean up ugly stuff in the url
    //                                match = (match =~ /\\?null=&/).replaceAll('')
    //                                match = (match =~ /&id=$/).replaceAll('')
                                    log(' found default http stream url, not matched for quality: ' + match)
                                    // Nota bene: this is most likely a f4m manifest, which is not playable in ffmpeg.
                                    // Need to work on hds decoding?
                                    // Ticket #1964 has been opened with ffmpeg!: 
                                    // https://ffmpeg.org/trac/ffmpeg/ticket/1964
                                }
    
                            }
                        }
                    }
                }
        }
        
        return match
    }

    Date GetValidDate(strMarkupDate)
    {
        // Reformat date from html markup to parsable date string
        //NOTE: SimpleDateFormat uses GMT[-+]hh:mm for the TZ which breaks
        //things a bit.  Before we go on we have to repair this.

        //this is zero time so we need to add that TZ indicator for 
        if ( strMarkupDate.endsWith( "Z" ) ) {
            strMarkupDate = strMarkupDate.substring( 0, strMarkupDate.length() - 1) + "GMT-00:00";
        } else {
            int inset = 6;
        
            String s0 = strMarkupDate.substring( 0, strMarkupDate.length() - inset );
            String s1 = strMarkupDate.substring( strMarkupDate.length() - inset, strMarkupDate.length() );

            strMarkupDate = s0 + "GMT" + s1;
        }
        
        // println 're-structured strRelease: ' + strReleaseDate
        
        return df.parse( strMarkupDate );
    }
    
    static main(args) {
        // this is just to test
//        def TestUrlHttp = new URL("http://www.oppetarkiv.se/etikett/titel/Fem%20myror%20%C3%A4r%20fler%20%C3%A4n%20fyra%20elefanter/")
//        def TestUrlHttp = new URL("http://www.oppetarkiv.se/etikett/titel/Fem%20myror%20%C3%A4r%20fler%20%C3%A4n%20fyra%20elefanter/?sida=2&sort=tid_stigande")
//        def TestUrlHttp = new URL("http://www.oppetarkiv.se/etikett/ovrigt/Beppe%20Wolgers/")
          def TestUrlHttp = new URL("http://www.oppetarkiv.se/etikett/titel/Djursjukhuset/")
        
//        def TestUrlByLetter = new URL("http://www.oppetarkiv.se/kategori/titel#0-9")        
        def TestUrlByLetter = new URL("http://www.oppetarkiv.se/program#G")        

        OppetArkiv extractor = new OppetArkiv()
        println "PluginName               : " + extractor.getExtractorName();
        println "Plugin version           : " + extractor.getVersion();
        println "TestMatch HTTP           : " + extractor.extractorMatches(TestUrlHttp);
    
        println ''
        
        assert extractor.extractorMatches( TestUrlHttp )
        assert !extractor.extractorMatches( new URL("http://google.com/feeds/api/standardfeeds/top_rated?time=today") )
        
        
        WebResourceContainer containerHttp = extractor.extractItems(TestUrlHttp, 28);

        ContentURLContainer result1Http = null
         if (containerHttp.getItems().size() > 0)
         {
            println ' containerHttp.getItems()[0].releaseDate: ' + containerHttp.getItems()[0].releaseDate
            result1Http = extractor.extractUrl(containerHttp.getItems()[0], PreferredQuality.LOW)

            for (int index = 0; index < containerHttp.getItems().size(); index++)
            {
                println 'http item #' + index + ': ' + containerHttp.getItems()[index].title + ', videoArticleUrl: ' + containerHttp.getItems()[index].getAdditionalInfo()['videoArticleUrl']
            }
         }
//        ContentURLContainer result1Http = extractor.extractUrl(containerHttp.getItems()[0], PreferredQuality.LOW)
        ContentURLContainer result2Http = extractor.extractUrl(containerHttp.getItems()[1], PreferredQuality.MEDIUM)
        ContentURLContainer result3Http = extractor.extractUrl(containerHttp.getItems()[2], PreferredQuality.HIGH)
        println "Result low HTTP: $result1Http"
        println "Result medium HTTP: $result2Http"
        println "Result high HTTP: $result3Http"

        WebResourceContainer containerHttpByLetter = extractor.extractItems(TestUrlByLetter, -1);

        ContentURLContainer result1HttpByLetter = null
         if (containerHttpByLetter.getItems().size() > 0)
         {
            println ' containerHttpByLetter.getItems()[0].releaseDate: ' + containerHttpByLetter.getItems()[0].releaseDate
            result1HttpByLetter = extractor.extractUrl(containerHttpByLetter.getItems()[0], PreferredQuality.LOW)

            for (int index = 0; index < containerHttpByLetter.getItems().size(); index++)
            {
                println 'By letter item #' + index + ': ' + containerHttpByLetter.getItems()[index].title + ', videoArticleUrl: ' + containerHttpByLetter.getItems()[index].getAdditionalInfo()['videoArticleUrl']
            }
         }
//        ContentURLContainer result1Http = extractor.extractUrl(containerHttp.getItems()[0], PreferredQuality.LOW)
        
        ContentURLContainer result2HttpByLetter = (containerHttpByLetter.getItems().size() > 1) ? extractor.extractUrl(containerHttpByLetter.getItems()[1], PreferredQuality.MEDIUM) : extractor.extractUrl(containerHttpByLetter.getItems()[0], PreferredQuality.MEDIUM)
        ContentURLContainer result3HttpByLetter = (containerHttpByLetter.getItems().size() > 2) ? extractor.extractUrl(containerHttpByLetter.getItems()[2], PreferredQuality.HIGH) :  extractor.extractUrl(containerHttpByLetter.getItems()[0], PreferredQuality.HIGH)
        println "Result low by letter: $result1HttpByLetter"
        println "Result medium by letter: $result2HttpByLetter"
        println "Result high letter: $result3HttpByLetter"

        println ''
    }
}